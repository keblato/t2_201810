package testEstructuras;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.ListaDoblementeEncadenada;




public class pruebasLista<T> {


	private ListaDoblementeEncadenada<String> listaDoble ; 



	@Before
	public void setupEscenario1( )
	{
		listaDoble = new ListaDoblementeEncadenada();

		listaDoble.add("a");
		listaDoble.add("b");
		listaDoble.add("c");
		listaDoble.add("d");

	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}
	/**
	 * Abstract Data Type for a linked list of generic objects
	 * This ADT should contain the basic operations to manage a list
	 * add: add a new element T 
	 * delete: delete the given element T 
	 * get: get the given element T (null if it doesn't exist in the list)
	 * size: return the the number of elements
	 * get: get an element T by position (the first position has the value 0) 
	 * listing: set the listing of elements at the firt element
	 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
	 * next: advance to next element in the listing (return if it exists)
	 * @param <T>
	 */


	@Test
	public void testAdd( )
	{
		setupEscenario1();
		assertTrue( "", listaDoble.get(0).equals("a"));
		assertTrue( "", listaDoble.get(3).equals("d"));

	}
	@Test
	public void testDelete( )
	{
		setupEscenario1();
		listaDoble.remove("b");
		System.out.println(listaDoble.get("b"));
		assertTrue( "", listaDoble.get("b") == null );
		
		
		listaDoble.remove("a");
		assertTrue( "", listaDoble.get(0).equals("c") );
		
		listaDoble.add("f");
		listaDoble.remove("d");
		assertTrue( "", listaDoble.get(listaDoble.size()-1).equals("f") );

		

		



	}
	@Test
	public void testGet1( )
	{
		setupEscenario1();

		listaDoble.add("H");
		
		assertTrue( "", listaDoble.get("H").equals("H") );	 	 
	}
	@Test
	public void testSize( )
	{
		setupEscenario1();

		listaDoble.add("asd");
		assertTrue( "", listaDoble.size() == 5);



	}
	@Test
	public void testGet2( )
	{
		setupEscenario1();

		listaDoble.add("mala");
		assertTrue( "", listaDoble.get(4) == "mala" );
		assertTrue( "", listaDoble.get(0) == "a" );


	}
	@Test
	public void testListinig( )
	{


	}
	@Test
	public void testGetCurrent( )
	{
		setupEscenario1();
		
		listaDoble.next();
		assertTrue( "", listaDoble.getCurrent().equals("b") );
		listaDoble.next();
		assertTrue( "", listaDoble.getCurrent().equals("c") );
		listaDoble.next();
		assertTrue( "", listaDoble.getCurrent().equals("d") );
		listaDoble.next();
		System.out.println(listaDoble.getCurrent());
		assertTrue( "", listaDoble.getCurrent()== null );
		listaDoble.next();
		listaDoble.next();
		assertTrue( "", listaDoble.getCurrent()== null );





		
		
	}
	@Test
	public void testNext( )
	{

	}

}
