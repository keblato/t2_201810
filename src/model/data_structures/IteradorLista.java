package model.data_structures;

import java.io.Serializable;
import java.util.ListIterator;

/**
 * Clase que representa el iterador de lista (avanza hacia adelante y hacia atr�s)
 * @param <E> Tipo del objeto que almacena el iterador de la lista
 */
public class IteradorLista<E extends Comparable<E>> implements ListIterator<E> 
{


	/**
	 * Nodo anterior al que se encuentra el iterador.
	 */
	private NodoListaDoble<E> anterior;

	/**
	 * Nodo en el que se encuentra el iterador.
	 */
	private NodoListaDoble<E> actual;
	
	/**
     * Crea un nuevo iterador de lista iniciando en el nodo que llega por par�metro
     * @param primerNodo el nodo en el que inicia el iterador. nActual != null
     */
	public IteradorLista(NodoListaDoble<E> primerNodo)
	{
		actual = primerNodo;
		anterior = null;
	}
	
    /**
     * Indica si hay nodo siguiente
     * true en caso que haya nodo siguiente o false en caso contrario
     */
	public boolean hasNext() 
	{
		// TODO Completar seg�n la documentaci�n

		boolean haySiguiente = false;
		if ( actual != null && actual.darSiguiente()!= null){
			haySiguiente = true;
		}
		
		return haySiguiente;
	}

    /**
     * Indica si hay nodo anterior
     * true en caso que haya nodo anterior o false en caso contrario
     */
	public boolean hasPrevious() 
	{
		// TODO Completar seg�n la documentaci�n
		boolean haySiguiente = false;
		if ( actual != null && actual.darAnterior()!= null){
			haySiguiente = true;
		}
		
		return haySiguiente;
	}

    /**
     * Devuelve el elemento siguiente de la iteraci�n y avanza.
     * @return elemento siguiente de la iteraci�n
     */
	public E next() 
	{
		// TODO Completar seg�n la documentaci�n
		E elemento = actual.darElemento();
		anterior = actual;
		if (actual != null){
		actual = (NodoListaDoble<E>) actual.darSiguiente();
		}
		return elemento;
	}

    /**
     * Devuelve el elemento anterior de la iteraci�n y retrocede.
     * @return elemento anterior de la iteraci�n.
     */
	public E previous() 
	{
		// TODO Completar seg�n la documentaci�n
		E elemento = actual.darElemento();
		actual = anterior;
		if (anterior != null){
		anterior = anterior.darAnterior();
		}
		
		return elemento;
		
	}
	
	
	//=======================================================
	// M�todos que no se implementar�n
	//=======================================================
	
	public int nextIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public int previousIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public void remove() 
	{
		throw new UnsupportedOperationException();
	}

	public void set(E e) 
	{
		throw new UnsupportedOperationException();
	}
	
	public void add(E e) 
	{
		throw new UnsupportedOperationException();		
	}

}
