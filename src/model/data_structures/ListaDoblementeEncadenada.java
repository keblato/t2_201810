package model.data_structures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;




/**
 * Clase que representa la lista doblemente encadenada
 * @param <E> Tipo de los objetos que almacenar� la lista.
 */
public class ListaDoblementeEncadenada<E extends Comparable<E>> extends ListaEncadenadaAbstracta<E>
{

	private NodoListaDoble<E> actual;
	/**
	 * Construye una lista vacia
	 * <b>post:< /b> se ha inicializado el primer nodo en null
	 */
	public ListaDoblementeEncadenada() 
	{
		primerNodo = null;
		cantidadElementos = 0;
		actual = null;
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public ListaDoblementeEncadenada(E nPrimero)
	{
		if(primerNodo == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		primerNodo = new NodoListaDoble<E>(nPrimero);
		actual = (NodoListaDoble<E>) primerNodo;
		cantidadElementos = 1;
	}

	/**
	 * Agrega un elemento al final de la lista
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id.
	 * Se actualiza la cantidad de elementos.
	 * @param e el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public boolean add(E e) 
	{
		// TODO Completar seg�n la documentaci�n
		boolean seAgrego = false;
		if (actual == null){
			actual = (NodoListaDoble<E>) primerNodo;
		}
		if (e == null){
			throw new NullPointerException();
		}

		NodoListaDoble<E> ultimo = darUltimoNodo();

		NodoListaDoble<E> nuevo = new NodoListaDoble<E>(e);


		if (ultimo != null){
			nuevo.cambiarAnterior(ultimo);
			ultimo.cambiarSiguiente(nuevo);
			seAgrego = true;	

		}
		else {
			primerNodo = nuevo;
			seAgrego = true;

		}


		if(seAgrego)
			cantidadElementos++;

		return seAgrego;

	}



	public NodoListaDoble<E> darUltimoNodo() {
		// TODO Auto-generated method stub
		if (primerNodo != null){
			NodoListaSencilla<E> actual = primerNodo;

			while (actual != null){
				if (actual.darSiguiente() == null){
					NodoListaDoble<E> a = (NodoListaDoble<E>) actual;
					return a ;
				}
				actual = actual.darSiguiente();
			}
		}
		return null;

	}

	/**
	 * Agrega un elemento al final de la lista. Actualiza la cantidad de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param elemento el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public void add(int index, E elemento) 
	{
		// TODO Completar seg�n la documentaci�n

		if (actual == null){
			actual = (NodoListaDoble<E>) primerNodo;
		}
		if (elemento == null){
			throw new NullPointerException();
		}

		NodoListaDoble<E> anterior = (NodoListaDoble<E>) get(index-1);
		NodoListaDoble<E> actual = (NodoListaDoble<E>) get(index-1);
		NodoListaDoble<E> nuevo = new NodoListaDoble<E>(elemento);



		if (actual != null){
			if (anterior != null){
				actual.cambiarAnterior(nuevo);
				nuevo.cambiarSiguiente(actual);
				anterior.cambiarSiguiente(nuevo);
				nuevo.cambiarAnterior(anterior);
			}else {
				nuevo.cambiarSiguiente(actual);
				primerNodo = nuevo;
				actual.cambiarAnterior(nuevo);

			}
		}else {

			primerNodo = nuevo;

		}

		cantidadElementos++;

	}

	/**
	 * M�todo que retorna el iterador de la lista.
	 * @return el iterador de la lista.
	 */
	public ListIterator<E> listIterator() 
	{
		return new IteradorLista<E>((NodoListaDoble<E>) primerNodo);
	}

	/**
	 * M�todo que retorna el iterador de la lista desde donde se indica.
	 * @param index �ndice desde se quiere comenzar a iterar.
	 * @return el iterador de la lista.
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public ListIterator<E> listIterator(int index) 
	{
		if(index< 0 || index >= size())
			throw new IndexOutOfBoundsException("El �ndice buscado est� por fuera de la lista.");
		return new IteradorLista<E>((NodoListaDoble<E>) darNodo(index));
	}

	/**
	 * Elimina el nodo que contiene al objeto que llega por par�metro.
	 * Actualiza la cantidad de elementos.
	 * @param seElimino 
	 * @param objeto el objeto que se desea eliminar. objeto != null
	 * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
	 */
	@Override
	public boolean remove(E o) 
	{

		boolean seElimino = false;
		// TODO Completar seg�n la documentaci�n
		int index = indexOf(o);
		NodoListaDoble<E> actual = null;
		NodoListaDoble<E> anterior = null;

		if (index >= 0){
			actual =  (NodoListaDoble<E>) darNodo(index);
			if (index-1 >= 0){
				anterior =  (NodoListaDoble<E>) darNodo(index-1);
			}	 

		}


		if (actual != null){
			NodoListaDoble<E> siguiente = (NodoListaDoble<E>) actual.darSiguiente();

			if (anterior != null){
				if (siguiente != null){
					anterior.cambiarSiguiente(siguiente);
					siguiente.cambiarAnterior(anterior);
				}else{
					anterior.cambiarSiguiente(null);
				}

				seElimino = true;
			}
			else {
				if (siguiente != null){
					siguiente.cambiarAnterior(null);
					primerNodo = siguiente;
					seElimino = true;
				}else{
					primerNodo= null;
					seElimino = true;
				}
			}
		}else {
			seElimino = false;
		}

		if (seElimino)
			cantidadElementos--;

		return seElimino;
	}

	/**
	 * Elimina el nodo en la posici�n por par�metro.
	 * Actualiza la cantidad de elementos.
	 * @param pos la posici�n que se desea eliminar
	 * @return el elemento eliminado
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public E remove(int index) 
	{
		// TODO Completar seg�n la documentaci�n
		E elemento = null;
		NodoListaDoble<E> actual = null;
		NodoListaDoble<E> anterior = null;
		actual = (NodoListaDoble<E>) darNodo(index);
		if (index-1 >= 0){
			anterior = (NodoListaDoble<E>) darNodo(index-1);
		}

		if (actual != null){
			if (anterior != null){
				NodoListaDoble<E> siguiente = (NodoListaDoble<E>) actual.darSiguiente();
				anterior.cambiarSiguiente(siguiente);
				if (siguiente != null){
					siguiente.cambiarAnterior(anterior);
				}

				elemento = actual.darElemento();
			}
			else{
				primerNodo = null;
				elemento = actual.darElemento();
			}
		}
		cantidadElementos--;

		return elemento; 

	}

	/**
	 * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro.
	 * Actualiza la cantidad de elementos
	 * @param coleccion la colecci�n de elementos a mantener. coleccion != null
	 * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
	 */
	public boolean retainAll(Collection<?> c) 
	{
		// TODO Completar seg�n la documentaci�n
		Object[] lista = c.toArray();
		ArrayList <Integer> elementos = new ArrayList<>();
		boolean funciona = true;
		for (int i = 0; i < lista.length; i++) {
			elementos.add(indexOf(lista[i]));
		}
		Collections.sort(elementos);
		for (int i = 0 ; i < size() && funciona; i++){
			if (!elementos.contains(i) ){
				remove(i);
				if (contains(i)){
					funciona = false;
				}
			}

		}
		cantidadElementos = elementos.size();
		return funciona;
	}

	/**
	 * Crea una lista con los elementos de la lista entre las posiciones dadas
	 * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
	 * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
	 * @return una lista con los elementos entre las posiciones dadas
	 * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
	 */
	public List<E> subList(int inicio, int fin) 
	{
		// TODO Completar seg�n la documentaci�n
		List <E> lista = new ArrayList<E>();
		if (inicio < 0 || fin >= size() || fin < inicio){
			throw new IndexOutOfBoundsException();
		}
		while (inicio < fin){
			lista.add(get(inicio));
			inicio--;	
		}

		return lista;
	}



	@Override
	public E get(E elemento) {
		// TODO Auto-generated method stub
		if (primerNodo != null) {

			if (primerNodo.darElemento().compareTo(elemento)==0) {
				return primerNodo.darElemento();
			}else {
				Iterator<E> iterador = listIterator();
				int i = 0;
				while (i < cantidadElementos){
					E siguiente =  iterador.next();

					if (siguiente.compareTo(elemento)== 0)
						return siguiente;
					i++;
				}

			}


		}




		return null;
	}

	@Override
	public void listing() {
		// TODO Auto-generated method stub
		actual = (NodoListaDoble<E>) primerNodo;
	}

	@Override
	public E getCurrent() {
		// TODO Auto-generated method stub
		if (actual == null )
			return null;
		return actual.darElemento();
	}


	// Se puede avanzar con un iterador o con next como prefiera
	public E next() {
		// TODO Auto-generated method stub
		if (actual == null){
			return null;
		}		
		else{
			actual = (NodoListaDoble<E>) actual.darSiguiente();	
			if (actual == null)
				return null;
		}
		return actual.darElemento();
	}


	public E previous() {
		// TODO Auto-generated method stub
		actual =  actual.darAnterior();	

		if (actual == null)
			return null;

		return actual.darElemento();
	}

	@Override
	public E get(int index) {

		NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo;
		if (index < 0 || index >= size()){
			throw new IndexOutOfBoundsException();
		}
		for (int i = 0; i < cantidadElementos; i++) {

			if (i == index){
				return actual.darElemento();
			}
			actual = (NodoListaDoble<E>) actual.darSiguiente();
		}

		return null;
	}










}


